﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FpsDisp : MonoBehaviour
{
    int frameCount;
    float prevTime;
    float currentFps;

	[SerializeField]
	Vector2 basePos = new Vector2(0, 0);
	[SerializeField]
	float width = 200;

    void Start()
    {
        frameCount = 0;
        prevTime = 0.0f;
        currentFps = 0.0f;
    }

    void Update()
    {
        frameCount++;
        float time = Time.realtimeSinceStartup - prevTime;

        if (time >= 0.5f)
        {
            currentFps = frameCount / time;
            frameCount = 0;
            prevTime = Time.realtimeSinceStartup;
        }
    }

    void OnGUI()
    {
        // GUI.TextAreaで指定する範囲は、Pixel単位

        // deltaTime表示
		GUI.TextArea(new Rect(basePos.x + 0, basePos.y + 0, width, 20), "deltaTime : " + Time.deltaTime);
        // fixedDeltaTime表示
		GUI.TextArea(new Rect(basePos.x + 0, basePos.y + 20, width, 20), "fixedDeltaTime : " + Time.fixedDeltaTime);

        // deltaTimeから毎フレーム計算
        float fps = Mathf.Round(1 / Time.deltaTime * 100) / 100;
		GUI.TextArea(new Rect(basePos.x + 0, basePos.y + 50, width, 20), "計算① : " + fps.ToString("F2") + " FPS");
        // 一定時間毎にFPS計算
        GUI.TextArea(new Rect(basePos.x + 0, basePos.y + 70, width, 20), "計算⑵ : " + currentFps.ToString("F2") + " FPS");
    }
}