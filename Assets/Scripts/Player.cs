﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour
{
	[SerializeField]
	float downSpeed;

	public UnityEngine.UI.Text scoreValue;
	public UnityEngine.UI.Image nekokanImage;
	public AudioClip[] sounds;
	public bool IsStop;

	public SpriteRenderer nekobako;
	public Sprite[] nekobakoImage;

	Rigidbody2D rb;
    Animator animCtrl;
    AudioSource audioSource;

	// Score
    int score;
    int Score
    {
        set
        {
            score = value;
            scoreValue.text = score.ToString();
        }
        get
        {
            return score;
        }
    }

	private void OnTriggerEnter2D(Collider2D collision)
	{
		//Destroy(collision.gameObject);
		collision.gameObject.SetActive(false);
		if (collision.name == "nekokan")
		{
			nekokanImage.gameObject.SetActive(true);
			audioSource.PlayOneShot(sounds[2]);
		}
		else
		{
			Score++;
			audioSource.PlayOneShot(sounds[1]);
		}
	}

	public void Reset()
	{
		transform.position = new Vector3(0, -1.61f, 0);
		downSpeed = 0;
		Score = 0;
		IsStop = true;
		animCtrl.SetTrigger("DemoEnd");
		nekokanImage.gameObject.SetActive(false);
		nekobako.sprite = nekobakoImage[0];
	}

	public void DemoStart()
	{
		animCtrl.SetTrigger("DemoStart");
	}

	// Use this for initialization
	void Start()
	{
		rb = GetComponent<Rigidbody2D>();
		animCtrl = GetComponent<Animator>();
		audioSource = GetComponent<AudioSource>();

		Reset();
	}

	// Update is called once per frame
	void Update()
	{
		float fowardSpeed;

		if (IsStop){
			fowardSpeed = 0;
		}
		else{
			fowardSpeed = 1;
		}

		RaycastHit2D hit;
		// 床等と接触しているか判定
		hit = Physics2D.Raycast(transform.position + new Vector3(-0.32f, -0.32f), Vector2.right, 0.64f);
		// Rayをデバッグ表示できる
		Debug.DrawRay(transform.position + new Vector3(-0.32f, -0.32f), Vector2.right * 0.64f, Color.green);
		if (hit.transform != null)
		{
			// 重力はクリア
			downSpeed = 0;

			// ジャンプボタン押下された？
			if (Input.GetButtonDown("Jump") && !IsStop)
			{
				downSpeed = 6.5f;
				// 位置更新される前に次のUpdate()が呼ばれるとジャンプがキャンセルされるのを回避
				transform.Translate(Vector3.up * 0.01f);
				audioSource.PlayOneShot(sounds[0]);
			}
			animCtrl.SetBool("IsGround", true);
		}
		else
		{
			// ジャンプ・落下中は重力計算
			downSpeed += -0.3f;
			animCtrl.SetBool("IsGround", false);
		}

		// 天井等と接触しているか判定
		hit = Physics2D.Raycast(transform.position + new Vector3(-0.32f, 0.32f), Vector2.right, 0.64f);
		Debug.DrawRay(transform.position + new Vector3(-0.32f, 0.32f), Vector2.right * 0.64f, Color.green);
		if (hit.transform != null)
		{
			downSpeed = -0.1f;
			transform.position += new Vector3(0, -0.1f, 0);
		}

		// 前方方向との接触チェック
		// 地面・自身のColliderと接触しないように調整
		hit = Physics2D.Raycast(transform.position + new Vector3(0.34f, 0.26f), Vector2.down, 0.52f);
		Debug.DrawRay(transform.position + new Vector3(0.34f, 0.26f), Vector2.down * 0.52f, Color.green);
		if (hit.transform != null || IsStop)
		{
			animCtrl.SetBool("IsIdol", true);
		}
		else
		{
			animCtrl.SetBool("IsIdol", false);
		}

		// プレイヤーの位置更新
#if true
		Vector2 nowpos = rb.position;
		nowpos += new Vector2(fowardSpeed, downSpeed) * Time.deltaTime;
		rb.MovePosition(nowpos);
#else
		Vector3 nowpos = transform.position;
        nowpos += new Vector3(fowardSpeed, downSpeed, 0) * Time.deltaTime;
        transform.position = nowpos;
#endif
		//		Debug.Log("nowpos: " + nowpos);

		animCtrl.SetFloat("DownSpeed", downSpeed);
	}

	void playJumpSound(){
		audioSource.PlayOneShot(sounds[0]);	
	}

	void changeNekobako(){
		nekobako.sprite = nekobakoImage[1];
	}
}
